<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class InfluxdbController extends BaseController
{
    protected $httpCliente;
    protected $url = 'http://localhost:8086/write?db=teste';

    public function __construct()
    {
        $this->httpCliente = new \GuzzleHttp\Client();
    }

    public function index()
    {
        $insert = 'abc a=1.1,b=2.6';

        $this->writeInfluxDB($insert);

        return 'Gravou';
    }

    public function writeInfluxDB($insert)
    {
        try {
            $response = $this->httpCliente->request('POST', $this->url, ['body' => $insert]);
        } catch (\Exception $e) {

            if (!$e->hasResponse()) {
                throw new \Exception('Error Write Send', 1);
            }
            $response = $e->getResponse();
        }

        return $response;
    }
}
